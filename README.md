## Configuração automatizada do rvm - ruby version manager
---

- Instale o git

```bash

sudo apt install git -y

```
- Clone o Repositório

```bash

git clone https://codeberg.org/jesherminelli/rvm-autoinstall.git

```

- Execute o script

```bash

sudo ./rvm-autoinstall

```
